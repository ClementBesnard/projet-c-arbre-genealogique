#ifndef FAMILYTREE_H
#define FAMILYTREE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct f_member {
     void* value;
     struct f_member* spouse; // éventuel conjoint
     struct f_member* parent1;
     struct f_member* parent2;
     struct f_member* sibling; // frere ou soeur plus jeune
     struct f_member* child; // premier enfant 
     void (*print)(void*); // On stocke la fonction d'affichage
} ;

typedef struct f_member* family_member;


struct family_tree
{
     family_member* root_node_list; // liste des membres racines
     int size; // nombre d'éléments
};

typedef struct family_tree* family_tree;



family_tree init_tree();

void add_root(family_tree, family_member);

void delete_tree(family_tree*, bool (*is_same)(void *m, void *m1));

family_member create_node( void* value, void (*print)(void* md) );

void add_child( family_member parent1, family_member parent2, family_member child );

void add_sibling( family_member elder, family_member sibling );

void the_wedding_present( family_member sp1, family_member sp2 );

family_member search( family_member haystack, void* needle, bool (*is_same)(void*, void*) );

void show( family_member fm );

void delete_from_node(family_member*, bool (*is_same)(void *m, void *m1));


#endif