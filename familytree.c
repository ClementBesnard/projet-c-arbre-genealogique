#include "familytree.h"




/**
    Cette fonction initialise un noeud membre de la famille
    à partir d'un personnage
    Paramètres :
    - Un pointeur sur un personnage : void*
    - un pointeur sur une fonction d'affichage : p
    Retourne un pointeur sur le membre de la famille correspondant au
    personnage passé en paramètre
*/

family_member create_node( void* value, void p(void* md) )
{
    family_member fm;
    fm = (family_member) malloc( sizeof(struct f_member) );
    if (fm == NULL)
    {
        printf("Unallocated memory\n");
    }
    
    fm->value = value;
    fm->spouse  = NULL;
    fm->parent2  = NULL;
    fm->parent1  = NULL;
    fm->sibling = NULL;
    fm->child   = NULL;
    fm->print = p;
    printf("Create node : ");
    p(fm->value);
    printf("\n");
    return fm;
}

/**
    Cette fonction ajoute un frere ou une soeur à un enfant existant
    Paramètres :
    - pointeur sur un membre de la famille auquel on veut attribuer un
      frere ou une soeur
    - pointeur sur le frere ou la soeur à rajouter
    Ne retourne rien
*/

void add_sibling( family_member elder, family_member sibling )
{
    if( elder != NULL ) {
        int i = 1;
        while( elder->sibling != NULL ) { // recherche du dernier de la fratrie
            elder = elder->sibling;
            i++;
        }
        elder->sibling = sibling;
    }
}

/**
    Cette fonction attribue un fils ou une fille à un ou deux parents
    Paramètres :
    - parent1 : pointeur sur le premier parent, membre de la famille
    - parent2 : pointeur sur le deuxième parent, membre de la famille
    - child : membre de la famille attribué comme enfant du couple ou du parentx
    Ne retourne rien
*/

void add_child( family_member parent1, family_member parent2, family_member child )
{
    if( parent1 != NULL ) {
        child->parent1 = parent1;
        if( parent1->child == NULL ) {
            parent1->child = child;
        }
        else {
            add_sibling( parent1->child, child );
        }
    }
    if( parent2 != NULL ) {
        child->parent2 = parent2;
        if( parent2->child == NULL ) {
            parent2->child = child;
        }
        else if( parent1 == NULL ) { /// pas de famille recomposée
            add_sibling( parent2->child, child );

        }
    }
}

/**
    Cette fonction permet de "marier" deux membres de la famille
    Paramètres :
    - sp1 premier membre du couple
    - sp2 deuxième membre du couple
    Ne retourne rien
*/

void the_wedding_present( family_member sp1, family_member sp2 )
{
    if( sp1->spouse == NULL && sp2->spouse == NULL ) {
        sp1->spouse = sp2;
        sp2->spouse = sp1;

    }
    else
    {
        printf("Members are already married");
    }
    
    
}

/**
    Cette fonction recherche un membre de la famille à partir d'un autre
    membre de la famille, c'est à dire qu'elle permet de vérifier si
    un membre de la famille fait partie de la famille (descendante)
    d'un autre membre
    Paramètres :
    - haystack : Membre de la famille à partir duquel on va chercher
    - needle : membre de la famille cherché
    Retourne NULL si needle n'est pas trouvé, needle si celui-ci fait partie
        de la famille de haystack
*/

family_member search( family_member haystack, void* needle, bool (*is_same)(void*, void*) )
{   

    family_member member = NULL;
    if( haystack == NULL ) return NULL;
    else {
        if( (*is_same)(haystack->value, needle) ) return haystack;
        else {
            if( haystack->child != NULL ) {
                member = search(  haystack->child,  needle, is_same );
            }
            if( haystack->sibling != NULL ) {
                member = search(  haystack->sibling,  needle, is_same );
            }


        }
    }
    return member;
}

/**
    Cette fonction permet l'affichage d'un arbre généaloqique descendant
    à partir d'un membre de la famille
    Cette fonction es récursive
    Paramètres :
    fm : Pointeur sur le membre de la famille à patir duquel on veut l'arbre
    Ne retourne rien
*/

void show( family_member fm ) {
    static int nb = 0; // statique : ce sera la même dans toutes les récursions
    if( nb == 0 ) printf("\n\nArbre Généalogique : \n");
    if( fm == NULL ) {
        printf("\nInvalid node !\n");
        return ;
    }
    else {
        for(int i=0;i<nb-1;i++) printf(" |  ");
        if(nb>0) printf(" |--");
        if( fm->value != NULL ) fm->print(fm->value);

        if( fm->spouse != NULL ) {
            printf(" <===>  ");
            fm->print( fm->spouse->value );
        }
        printf("\n");
    }
    if( fm->child != NULL ) {
        nb++; // on va afficher un enfant : on fait un niveau de plus
        show( fm->child );
        nb--; // on a affiché un enfant : on fait un niveau de moins
    }
    if( fm->sibling != NULL ) {
        show( fm->sibling );
    }

}

/**
    Cette fonction détruit les membres de la famille descendants
    du membre de la famille donné en paramètre
    Cette fonction est récursive
    Paramètres :
    fm : Pointeur sur le membre de la famille à patir duquel on veut détruire
    comp : pointer sur la fonction de comparaison
    Ne retourne rien
*/

static family_member first_member_del = NULL; // retient le premier membre à détruire


void delete_from_node(family_member* fm, bool (*is_same)(void *m, void *m1)){
    
    bool res = false;
    if (*fm != NULL)
    {   

        // on stocke le premier membre
        if (!first_member_del)
        {   
            first_member_del = *fm;
        }

        delete_from_node(&(*fm)->child,is_same); // appel récursif sur les enfants
        
        if (!is_same(first_member_del->value, (*fm)->value))
        {
            delete_from_node(&(*fm)->sibling,is_same); // appel récursif sur les frères si ce n'est pas le premier membre
        }
        else
        {
            // permets de garder la structure d'arbre lorsque l'on supprime le premier membre passé à la fonction
            res = true;
            family_member last_sibling;
            bool is_first_child = false;
            if ((*fm)->parent2)
            {   
                last_sibling = (*fm)->parent2->child;
                if (is_same((*fm)->parent2->child->value, (*fm)->value))
                {
                    (*fm)->parent2->child = NULL;
                    if ((*fm)->sibling)
                    {   
                        (*fm)->parent2->child = (*fm)->sibling;
                    }

                }
                else
                {
                    last_sibling = (*fm)->parent2->child;
                    while (!is_same(last_sibling->sibling->value, (*fm)->value))
                    {
                        last_sibling = last_sibling->sibling;
                    }
                    is_first_child = true;
  
                }
                
            }
            if ((*fm)->parent1)
            {   
                last_sibling = (*fm)->parent1->child;
                if (is_same((*fm)->parent1->child->value, (*fm)->value))
                {
                    (*fm)->parent1->child = NULL;
                    if ((*fm)->sibling)
                    {

                        (*fm)->parent1->child = (*fm)->sibling;
                    }
                }
                else
                {   
                    last_sibling = (*fm)->parent1->child;
                    while (!is_same(last_sibling->sibling->value, (*fm)->value))
                    {
                        last_sibling = last_sibling->sibling;
                    }
                    is_first_child = true;
 
                }
                

            }
            if (is_first_child)
            {
                if ((*fm)->sibling)
                {   
                    last_sibling->sibling = (*fm)->sibling;
                }
                else
                {   
                    last_sibling->sibling = NULL;
                    
                    
                }
            }
        
        }
        // on place sur NULL les pointeurs de son conjoint
        if ((*fm)->spouse)
        {
            (*fm)->spouse->spouse = NULL;
            (*fm)->spouse->child = NULL;
        }
        

        free((*fm)->value);
        free((*fm));
        
        (*fm)=NULL;

        if (res)
        {
            first_member_del = NULL;
        }


    }
    
}

/**
    Cette fonction initialise une liste de racines qui composeront
    l'arbre généalogique d'un clan
    Pas de parametre en entrée
    Retourne un pointeur sur la liste pour le clan
*/

family_tree init_tree(){
    family_tree f_tree = malloc(sizeof(struct family_tree));
    if (f_tree == NULL)
    {
        printf("Memoire non allouee.\n");
    }
    
    f_tree->root_node_list = NULL;
    f_tree->size = 0;

    return f_tree;

}

/**
    Cette fonction ajoute une racine à l'arbre du clan
    Paramètres :
    - Un pointeur sur la liste de racines : root
    - Un pointeur sur une racine (membre famillial) : fm
    Ne retourne rien
*/

void add_root(family_tree ft, family_member fm){
    ft->size = ft->size + 1;
    // réalloue de la place pour le nouveau noeud racine
    family_member* tmp = (family_member*) realloc(ft->root_node_list, ft->size * sizeof(struct f_member));
    if (tmp == NULL)
    {
        printf("Can't reallocate memory\n");
    }
    else
    {
        ft->root_node_list = tmp;
    }
    
    
    ft->root_node_list[ft->size-1] = fm;
}

/**
    Cette fonction détruit toutes les racine d'un arbre généalogique
    Paramètres :
    - l'adresse d'un pointeur sur la liste de racine : ptree
    - un pointeur de fonction de comparaison : comp
    Ne retourne rien
*/

void delete_tree(family_tree* ft, bool (*is_same)(void *m, void *m1)){
    for (int i = 0; i < (*ft)->size; i++) // parcours des noeuds racines
    {   
        delete_from_node(&(*ft)->root_node_list[i], is_same); // détruire l'arbre à partir de chaque racine
    }

    free((*ft)->root_node_list);
    free((*ft));
    (*ft) = NULL;
    
}

