# compilateur
CC := gcc
# options de compilation
CFLAGS := -std=c99 -Wall -Wextra -pedantic -ggdb

# règle de compilation --- exécutables
all : main

main : main.o familytree.o
	$(CC) $(CFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

# options de compilation
memoire : main
	valgrind --track-origins=yes --leak-check=full ./main

clean:
	rm *.o main
